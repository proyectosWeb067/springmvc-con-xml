
package com.empresa.proyecto.dto;

 


public class PersonaDTO {
    
    private int idusuario;
    private String nombre;
    private String apellPat;
    private String apellMat;
    private String  tipodoc;
    private Integer numdoc;
    private Integer dianac;
    private String mesnac;
    private Integer anac;
    private boolean sexo;
    private Integer ruc;
    private String estadcivil;
    private String estadolabo;

    public PersonaDTO() {
    }

    public int getIdusuario(){
      return idusuario;
    }
    
    public void setIdusuario(int idusuario){
        this.idusuario = idusuario;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellPat() {
        return apellPat;
    }

    public void setApellPat(String apellPat) {
        this.apellPat = apellPat;
    }

    public String getApellMat() {
        return apellMat;
    }

    public void setApellMat(String apellMat) {
        this.apellMat = apellMat;
    }

    public String getTipodoc() {
        return tipodoc;
    }

    public void setTipodoc(String tipodoc) {
        this.tipodoc = tipodoc;
    }

    public Integer getNumdoc() {
        return numdoc;
    }

    public void setNumdoc(Integer numdoc) {
        this.numdoc = numdoc;
    }

    public Integer getDianac() {
        return dianac;
    }

    public void setDianac(Integer dianac) {
        this.dianac = dianac;
    }

    public String getMesnac() {
        return mesnac;
    }

    public void setMesnac(String mesnac) {
        this.mesnac = mesnac;
    }

    public Integer getAnac() {
        return anac;
    }

    public void setAnac(Integer anac) {
        this.anac = anac;
    }

    public boolean isSexo() {
        return sexo;
    }

    public void setSexo(boolean sexo) {
        this.sexo = sexo;
    }

    public Integer getRuc() {
        return ruc;
    }

    public void setRuc(Integer ruc) {
        this.ruc = ruc;
    }

    public String getEstadcivil() {
        return estadcivil;
    }

    public void setEstadcivil(String estadcivil) {
        this.estadcivil = estadcivil;
    }

    public String getEstadolabo() {
        return estadolabo;
    }

    public void setEstadolabo(String estadolabo) {
        this.estadolabo = estadolabo;
    }

    
    
}
