
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h4>Estas agregando un nuevo producto...</h4>
        
        <f:form modelAttribute="per" action="ejemplo.htm?accion=Inok" method="POST">
            
            <tr> 
                <td>Producto:</td>
                <td ><f:input path="titulo" /></td>
            </tr>  <br/><br/>
            <tr> 
                <td>Tipo:</td> 
                <td  ><f:input path="tipo"/></td> 
            </tr> <br/><br/>
            <tr> 
                <td>Precio:</td>  
                <td><f:input path="precio"/></td>
            </tr> <br/><br/>
            <tr> 
                <td>Descripción:</td>  
                <td><f:textarea path="descripcion" rows="5" cols="30"/> </td>
            </tr> <br/><br/>
            <tr>
                <input type="submit" value="Enviar"/>
            </tr> 
        </f:form>
     
         <footer class="page-footer background blue">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Descripción.</h5>
                <p class="grey-text text-lighten-4">Este es un proyecto, con fines educativos, 
                aplicando los conceptos del frameork spring MVC, con el framework materialize.</p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Links</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="#!">Bancolombiano</a></li>
                   
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2018 Copyright Text
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
            </div>
          </div>
        </footer>      
            
            
    </body>
</html>
