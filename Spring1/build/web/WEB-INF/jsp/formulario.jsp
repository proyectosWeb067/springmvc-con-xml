 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix = "c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
       
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <title>Targeta de Credito</title>
    </head>
    <body>
       <!-- <h1>Area de formulario !</h1>
       <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <h3 style="text-align: center;"> 
            --------------------------------------------------------------------           
            Solicitud de targeta 
            --------------------------------------------------------------------  
        </h3>
         -->
         
         <nav>
             <div class="nav-wrapper background blue">
                 <form>
                     <div class="input-field">
                         <a href="#"  class="brand-logo"><img src="img/bcolombia.jpg" style="max-height: 80px; max-width: 100px;"/></a>
                         <ul id="nav-mobile" class="right hide-on-med-and-down">
                             <li><a href="index.htm">Inicio</a></li>
                              
                         </ul>
                     </div>
                 </form>
             </div>
         </nav>
 
         
         <section class="container">
             <div class="row">
                  <h5>Bienvenido, por favor complete el formulario para revisar su solicitud.</h5>
                  <div class="col m10">    
                <f:form modelAttribute="persona" action="ejemplo.htm?accion=insok" method="POST">

                    <table>
                        <tr>
                            <td> Tipo de Documento: </td>
                            <td>
                                <f:radiobutton path="tipodoc" value="cc" id="cc" />
                                <f:label path="tipodoc" for="cc">CEDULA:</f:label>
                            </td>
                            <td>
                                <f:radiobutton path="tipodoc" value="ce" id="ce" />
                                <f:label path="tipodoc" for="ce">CEDULA EXTRANGERIA</f:label>
                            </td>
                        </tr>  
                        <tr>
                            <td>Numero de documento: </td>
                            <td><f:input path="numdoc" /></td>
                        </tr>
                        <tr>
                            <td>Apellido Paterno: </td>
                            <td><f:input path="apellPat" /></td>
                        </tr>
                        <tr>
                            <td>Apellido Materno: </td>
                            <td><f:input path="apellMat" /></td>
                        </tr>
                        <tr>
                            <td>Nombre: </td>
                            <td><f:input path="nombre" /></td> <!--apunta al atributo nombre de PersonaDTO-->
                        </tr>
                        <tr>
                            <td>Fecha de Nacimiento: </td>
                            <td>
                                <f:select path="dianac" items="${ld}"/>

                            </td>
                            <td>
                                <f:select path="mesnac" items="${lm}"/>

                            </td>

                            <td>
                                <f:select path="anac" items="${la}"/>

                            </td>
                        </tr>
                        <tr>
                                <td>Sexo: </td>
                                <td>
                                    <f:radiobutton path="sexo" value="true" id="Masculino" />
                                    <f:label path="sexo" for="Masculino">Masculino</f:label>
                                </td>
                                <td>
                                    <f:radiobutton path="sexo" value="false" id="Femenino" />
                                    <f:label path="sexo" for="Femenino">Femenino</f:label>
                                </td>
                        </tr>
                         <tr>
                                <td>Estado Civil:</td>
                                <td>
                                    <f:radiobutton path="estadcivil" value="s" id="s" />
                                    <f:label path="estadcivil" for="s">Soltero</f:label>
                                </td>
                                <td>
                                    <f:radiobutton path="estadcivil" value="c" id="c" />
                                    <f:label path="estadcivil" for="c">Casado</f:label>
                                </td> 

                                <td>
                                    <f:radiobutton path="estadcivil" value="d" id="d" />
                                    <f:label path="estadcivil" for="d">Divorciado</f:label>
                                </td> 

                       </tr>
                       <tr>
                                <td>Situación Laboral: </td>
                                <td>
                                    <f:radiobutton path="estadolabo" value="t" id="t" />
                                    <f:label path="estadolabo" for="t">Trabajando</f:label>
                                </td> 
                                <td>
                                    <f:radiobutton path="estadolabo" value="de" id="de" />
                                    <f:label path="estadolabo" for="de">Desempleado</f:label>
                                </td> 
                       </tr>   
                        <tr> 
                                <td>Ruc Empleador: </td>
                                <td><f:input path="ruc"/></td>
                       </tr>
                       <tr>    
                            <td>
                                <input class="waves-effect waves-light btn" type="submit" name="Enviar"/>
                            </td>

                       </tr>
                      <tr> <td></br> </br></td></tr>
                       <tr>
                       <td><a href="index.htm"> Regresar</a></td>
                    </tr>
                 </table>
               </f:form>
             </div>     
         </div>  
       </section>
         <footer class="page-footer background blue">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Descripción.</h5>
                <p class="grey-text text-lighten-4">Este es un proyecto, con fines educativos, 
                aplicando los conceptos del frameork spring MVC, con el framework materialize.</p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Links</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="#!">Bancolombiano</a></li>
                   
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2018 Copyright Text
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
            </div>
          </div>
        </footer>    
   </body>
</html>

                    
               
                