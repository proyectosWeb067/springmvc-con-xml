
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Productos</title>
    </head>
    <body>
        <nav>
            <div class="nav-wrapper">
                <a href="#"  class="brand-logo"><img src="img/bcolombia.jpg" style="max-height: 80px; max-width: 100px;"/></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a href="ejemplo.htm?accion=In">Agregar Producto</a></li>
                    <li><a href="badges.html">Editar Producto</a></li>
                    <li><a href="collapsible.html">Eliminar Producto</a></li>
                </ul>
            </div>
        </nav>
        
        <section class="container"> 
            
            <div class="row">
                <h3>Productos bancolombiano</h3>
               <table>
                    <thead>
                        <tr>
                            <td>Producto </td>
                            <td>Tipo</td>
                            <td>Precio</td>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <c:forEach var="p" items="${listapro}">
                            <tr>
                                <td>${p.titulo}</td>
                                <td>${p.tipo}</td>
                                <td>${p.precio}</td>
                                <th>
                                    <input type="checkbox" name="_del" 
                                           value="${p.idproducto}"/>
                                </th>
                                <th>
                                    <input type="radio" name="_upd" 
                                           value="${p.idproducto}"/>
                                </th>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
             <a href="index.htm">Regresar</a>
            </div>   
        </section>
    </body>
</html>
