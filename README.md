Descripcion
========================
SpringMVC con xml y jdbc
-----------------------

Este es un proyecto en su primera version, esta hecho con el framework 
spring 4, aplica el patron inversion de control con su tecnica inyeccion de dependencias, este proyecto esta 
configurado con xml para las inyeccion de dependencias en el archivo 
applicationcontext.xml, se utiliza matrialize como framework css, el patron DAO, DTO(objeto de 
transeferencia de datos).

La base de datos cuenta con solo dos simples tablas, esto porque se pretende de 
momento darle mas importancia a la parte de spring.

![Alt text] (springImg.jpg "imagen descipcion")
