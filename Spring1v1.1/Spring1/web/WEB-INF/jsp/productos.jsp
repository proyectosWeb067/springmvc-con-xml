
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <title>Productos</title>
    </head>
    <body onload="del()">
        <nav>
            <div class="nav-wrapper blue">
                <a href="#"  class="brand-logo"><img src="img/bcolombia.jpg" style="max-height: 80px; max-width: 100px;"/></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a href="ejemplo.htm?accion=In">Agregar Producto</a></li>
                    <li><a href="badges.html">Editar Producto</a></li>

                </ul>
            </div>
        </nav>

        <section class="container"> 

            <div class="row">
                <h3>Productos bancolombiano</h3>
                <table class="infoDel">
                    <thead>
                        <tr>
                            <td>Producto </td>
                            <td>Tipo</td>
                            <td>Precio</td>
                            <td class=" blue-text"><a class="elimi" href="#">Eliminar</a></td>
                        </tr>
                    </thead>

                    <tbody>
                        <c:forEach var="p" items="${listapro}">
                            <tr>
                                <td>${p.titulo}</td>
                                <td>${p.tipo}</td>
                                <td>${p.precio}</td>  
                                <td>
                                    <input  type="checkbox" name="del" id="${p.idproducto}" value="${p.idproducto}" />
                                    <label for="${p.idproducto}"/>  
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <a href="index.htm">Regresar</a>
            </div>   
        </section>

        <footer class="page-footer background blue">
            <div class="container">
                <div class="row">
                    <div class="col l6 s12">
                        <h5 class="white-text">Descripción.</h5>
                        <p class="grey-text text-lighten-4">Este es un proyecto, con fines educativos, 
                            aplicando los conceptos del frameork spring MVC, con el framework materialize.</p>
                    </div>
                    <div class="col l4 offset-l2 s12">
                        <h5 class="white-text">Links</h5>
                        <ul>
                            <li><a class="grey-text text-lighten-3" href="#!">Bancolombiano</a></li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <div class="container">
                    © 2018 Copyright Text
                    <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
                </div>
            </div>
        </footer>    


        <script type="text/javascript" src="js/materialize.js"></script>
        <script src="js/del.js" type="text/javascript"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

    </body>
</html>
