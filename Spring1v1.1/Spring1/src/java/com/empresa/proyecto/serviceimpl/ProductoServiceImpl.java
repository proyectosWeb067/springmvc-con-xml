 
package com.empresa.proyecto.serviceimpl;

import com.empresa.proyecto.dao.ProductosDao;
import com.empresa.proyecto.dto.ProductoDTO;
import com.empresa.proyecto.service.ProductoService;
 
import java.util.List;

 
public class ProductoServiceImpl implements ProductoService{
    //inyectando interfaz productos a el servicio, mirar xml en archivo applicationContxt.xml
     private ProductosDao pdao; 
     
     public void setProddaoimp(ProductosDao p){
        
         this.pdao = p;
     }
    
    @Override
    public List<ProductoDTO> getAll() {
        List<ProductoDTO> l;
         l = pdao.getAll();
        return l;
    }

    @Override
    public String insertarProducto(ProductoDTO p) {
        String m = pdao.insertarProducto(p);
             
        return m;
    }

    @Override
    public String eliminarProducto(List<Object[]> ids) {
        String res =  pdao.eliminarProducto(ids);
         return res;
    }

    @Override
    public String actualizarProducto(ProductoDTO p) {
        String res = pdao.actualizarProducto(p);
        return res;
    }

    @Override
    public ProductoDTO getProducto(Integer idproducto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
