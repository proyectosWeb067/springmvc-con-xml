package com.empresa.proyecto.controller;

import com.empresa.proyecto.dto.PersonaDTO;
import com.empresa.proyecto.dto.ProductoDTO;
import com.empresa.proyecto.service.ProductoService;
import com.empresa.proyecto.service.UsuarioService;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class EjemploController {
   
    //------------------------- INYECCION DE DEPENDENCIAS------------------------------------
    //inyectando interfaz productoservice a el controlador mirar xml en archivo applicationContxt.xml    
    //IoC (inversion de control, tecnica inyeccion de dependecias )
    private ProductoService pservice;
    private UsuarioService uservice;
     
    public void setProserimp(ProductoService pservic) {
        this.pservice = pservic ;
    }

    public void setUsserimp(UsuarioService uservice){
        this.uservice = uservice;
    }
    //------------------------------------------------------------------------------------------
    
    
     //---------------------------------C R U D - PRODUCTOS ---------------------------------------------
    @RequestMapping(params = "accion=lista")
    public ModelAndView listaProductos() {
        ModelAndView mav = grilla();
        return mav;
    }
    public ModelAndView grilla() {  
        List<ProductoDTO> lista = pservice.getAll();
        ModelAndView mav = new ModelAndView("productos");
       
        mav.addObject("listapro", lista);
      
        return mav;
    }
    
     //---- lista productos en pagina Nostros---
    @RequestMapping(params = "accion=nos-li")
    public ModelAndView listProdu(){
        ModelAndView mav = datosNosotros();
        return mav;
    }
    
   
    public ModelAndView datosNosotros(){
         List<ProductoDTO> l = pservice.getAll();
        ModelAndView mav = new ModelAndView("nosotros");
        mav.addObject("li", l);
        return mav;
    }
    //------------------------------------------------
    
    
    //-------------------insertando producto---------------------
    @RequestMapping(params = "accion=In")
    public ModelAndView insertProducto(){
        ModelAndView mav = new ModelAndView("insprod");
        
        ProductoDTO p = new ProductoDTO();
        mav.addObject("per", p);
    
        return mav;
    }
    @RequestMapping(params = "accion=Inok")
    public ModelAndView insertInfoProducto(@ModelAttribute ProductoDTO per){
        ModelAndView mav = new ModelAndView("salidaformulario");
        String r = pservice.insertarProducto(per);
          mav.addObject("info", r);
        
          return mav;
    }
 //-----------------------------------------------------------------------
    
  //-----------------------eliminando productos-----------------------------
  
 
  @RequestMapping(params = "accion=del")
  public ModelAndView deleteProducto(HttpServletRequest request ){
      String ids = request.getParameter("ids");
      ModelAndView mav = new ModelAndView("salidaformulario");
     
      List<Object[]> pids = new ArrayList();
      if (ids != null) {
          String[] idsOn = ids.split(",");
          
          for(String id : idsOn){
              pids.add(new Object[]{Integer.valueOf(id)});
              System.out.println("---->" +id);
          }
          
          String resp = pservice.eliminarProducto(pids);
           mav.addObject("meninfodelpro", resp);
      }
     
      return mav;
  }
  
   
 //----------------------------------------------------------------------------   
    
  //-------------------------------------------------------------------------
    
    
  //--------------------------------------------------------------------------------------
    
  //------------------------------------ C R U D - USUARIO -------------------------------
    
    
    @RequestMapping(params = "acc=getu")
    public ModelAndView listuser(){
        ModelAndView mav = getUsers();
        return mav;
    }
    
    public ModelAndView getUsers(){
        ModelAndView mav = new ModelAndView("uslist");
        List<PersonaDTO> l = uservice.getAll();
        mav.addObject("alluser",l);
        return mav;
    }
    
     //en el navegador ---->  ejemplo.htm?p01
    //Entrada de datos del controlador al jsp
    @RequestMapping(params = "p01")
    public ModelAndView Formulario1() {
        ModelAndView mv = new ModelAndView("formulario"); //pagina jsp
        PersonaDTO p = new PersonaDTO();
        mv.addObject("persona", p); // del controlador envio el objeto al jsp
        List<Integer> dia = new ArrayList<>();

        for (int i = 1; i <= 31; i++) {
            dia.add(i);
        }
        List<String> mes = new ArrayList<>();
        mes.add("Enero");
        mes.add("Febrero");
        mes.add("Marzo");
        mes.add("Abril");
        mes.add("Mayo");
        mes.add("Junio");
        mes.add("Julio");
        mes.add("Agosto");
        mes.add("Septiembre");
        mes.add("Octubre");
        mes.add("Noviembre");
        mes.add("Diciembre");

        List<Integer> a = new ArrayList<>();

        for (int i = 1900; i <= 2018; i++) {
            a.add(i);
        }

        mv.addObject("ld", dia);
        mv.addObject("lm", mes);
        mv.addObject("la", a);

        return mv;
    }
    
    @RequestMapping(params = "accion=insok")
    public ModelAndView insertUsuer(@ModelAttribute PersonaDTO persona){
        ModelAndView mav = new ModelAndView("salidaformulario");
       
        String eCiv = persona.getEstadcivil();
        String eLab = persona.getEstadolabo();
        switch (eCiv) {

            case "s":
               persona.setEstadcivil("Soltero");
                break;
            case "d":
               persona.setEstadcivil("Divorsiado");
                break;
            case "c":
                persona.setEstadcivil("Casado");;
                break;

            default:
                persona.setEstadcivil("Sin Dato");
                break;

        }

        switch (eLab) {

            case "t":
                persona.setEstadolabo("Trabajando");
                break;

            case "de":
                persona.setEstadolabo("Desempleado");
                break;

            default:
                persona.setEstadolabo("Sin Dato");
                break;

        }

        if (persona.getTipodoc().equals("cc")) {
            persona.setTipodoc("C.C");
        } else {
            persona.setTipodoc("C.E");
        }

         
     
        String resp =  uservice.insertarPersona(persona);
        mav.addObject("respuesta", resp);
        return mav;
    }
    
    
    //--------------------------------------------------------------------------------------
    
    @RequestMapping(params = "home")
    public ModelAndView nosotros(){
        ModelAndView mav = new ModelAndView("nosotros");
        
        return mav;
        
    }
    
    
   
    //salida de datos---> aqui ya estan seteados los valores del DTO y se espera la salida
    //prueba salida de datos capturados y enviados a jsp llamado(salidaformulario), como prueba 
    /*@RequestMapping(params = "s01")
    public ModelAndView salidaFormulario(@ModelAttribute PersonaDTO persona) {
        ModelAndView mv = new ModelAndView("salidaformulario");
        String mensaje;
        String meneC = "";
        String menL = "";
        String eCiv = persona.getEstadcivil();
        String eLab = persona.getEstadolabo();
        switch (eCiv) {

            case "s":
                meneC = "Soltero";
                break;
            case "d":
                meneC = "Divorciado";
                break;
            case "c":
                meneC = "Casado";
                break;

            default:
                meneC = "Sin Dato";
                break;

        }

        switch (eLab) {

            case "t":
                menL = "Trabajando";
                break;

            case "de":
                menL = "Desempleado";
                break;

            default:
                menL = "Sin Dato";
                break;

        }

        if (persona.getTipodoc().equals("cc")) {
            mensaje = "C.C";
        } else {
            mensaje = "C.E";
        }

        mv.addObject("meL", menL);
        mv.addObject("meC", meneC);
        mv.addObject("m", mensaje);
        mv.addObject("personasale", persona);
        return mv;
    }
    */
}
