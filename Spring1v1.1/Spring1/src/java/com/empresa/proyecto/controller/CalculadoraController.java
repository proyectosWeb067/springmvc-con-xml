
package com.empresa.proyecto.controller;

import com.empresa.proyecto.dto.CalDTO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CalculadoraController {
    
    @RequestMapping(params = "c1")
    public ModelAndView calc(){
        ModelAndView mv = new ModelAndView("calentrada");
        CalDTO c = new CalDTO();
       
        mv.addObject("valor", c);
   
        return mv;
    }
    //salida, que tiene carada la informaciona en el DTO
    @RequestMapping(params= "c2")
    public ModelAndView calsali(@ModelAttribute CalDTO valor){
        ModelAndView mv = new ModelAndView("calsalida");
        
        int val = 0;
        switch(valor.getOperacion()){
            
            case("suma"):
                  val = (valor.getNum1() + valor.getNum2());
                break;
            case("resta"):
                  val = (valor.getNum1() - valor.getNum2());
                break;
            case("multi"):
                  val = (valor.getNum1() * valor.getNum2());
                break;
            case("divi"):
                  val = (valor.getNum1() / valor.getNum2());
                break;
             
        }
 
        mv.addObject("valsalida", val);
        
        return mv;
    }
}
