 
package com.empresa.proyecto.dao;

import com.empresa.proyecto.dto.PersonaDTO;
import java.util.List;

 
public interface UsuarioDao {
    
   public List<PersonaDTO> getAll();
   public String insertarPersona(PersonaDTO p);
   public String eliminarPersona(List<Object[]> ids);
   public String actualizarPersona(PersonaDTO p);
   public PersonaDTO getPersona(Integer idpersona);
}
