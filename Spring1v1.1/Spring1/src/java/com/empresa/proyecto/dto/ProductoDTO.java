/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.proyecto.dto;

import java.io.Serializable;

/**
 *
 * @author juan
 */
public class ProductoDTO implements Serializable{
    
    private Integer idproducto;
    private String titulo;
    private String tipo;
    private Double precio;
    private String descripcion;
    
    public ProductoDTO(){
    }

    public Integer getIdproducto(){
        return idproducto;
    }
   
    public void setIdproducto(Integer idproducto){
        this.idproducto = idproducto;
    }
    
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
    
    
}
