

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Formulario Calculadora</h1>
        <f:form modelAttribute="valor" action="calc.htm?c2" method="POST">
            <table>
                <tr>
                    <td>Valor 1:</td>
                    <td><f:input path="num1"/></td>
                </tr>

                <tr>
                    <td>Valor 2:</td>
                    <td><f:input path="num2"/></td>
                </tr>

                <tr>
                <tr>
                    <td>Operacion:</td>
                    <td>
                        <f:select path="operacion">
                            <f:option value="suma">SUMA</f:option>
                            <f:option value="resta">RESTA</f:option>
                            <f:option value="multi">MULTIPLICACION</f:option>
                            <f:option value="divi">DIVISION</f:option>
                        </f:select> 
                    </td>    
                </tr>
            </tr>
            <tr>
                <td><input type="submit" name="Enviar"/></td>
            </tr>
        </table>
    </f:form>
</body>
</html>
