<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <link rel="stylesheet" type="text/css" href="css/estilonav.css" media="screen" />

        <title>Proyecto Bancolombiano</title>
    </head>

    <body>
        <header
            <nav>
                <img src="img/bcolombia.jpg"/>  
                <div class="contenedor">
                    <tr>
                        <td> <a href="ejemplo.htm?accion=nos-li">Nosotros </a> </td>&nbsp|&nbsp 
                        <td> <a href="">Servicio al cliente </a> </td>&nbsp|&nbsp 
                        <td> <a href="ejemplo.htm?p01">Targeta Credito </a> </td>&nbsp|&nbsp 
                        <td> <a href="ejemplo.htm?accion=lista">Productos</a> </td>&nbsp|&nbsp
                        <td> <a href="">Mapa de Sitio</a> </td>
                    </tr>
                    <div> 
                        </nav>

                        </header>
                        <section class="container">
                            <div class="contenedorTota">


                                <div class="contenido">
                                    <h1>Bienvenido, proyecto bancolombiano 1.0</h1>
                                    <div> 

                                        <p>Este es un proyecto realizado con fines  educativos y además con el proposito de 
                                            poner en practica algunos conceptos del framework spring con el cual se pueden desarrollar aplicativos web
                                            escalables y con una mayor rapides. las tecnologias que fueron implementadas para el desarrollo
                                            de este pequeño ejemplo funcional son: spring framework version 4, materialize, una pequeña base de datos
                                            la cual solo esta para persistir y servir los datos.
                                        </p>
                                        <p>
                                            En esta primera version se pretende poner en practica el diseño modelo, controlador, vista, servicio
                                            aplicando el patron Inversion de control con la tecnica de inyeccion de dependencias; esta version 
                                            esta configurada con xml, es decir se presenta al archivo applicationContext.xml las inyecciones de dependencia
                                            y respectivamente al archivo dispatcher-servlet.xml los controladores.</p>


                                    </div>
                                    <p>
                                    <h4>Usuarios que han solicitado targeta <a href="ejemplo.htm?acc=getu">ver</a></h4>
                                    </p>

                                </div> 


                            </div>  
                        </section>   
                        <footer class="page-footer background blue">
                            <div class="container">
                                <div class="row">
                                    <div class="col l6 s12">
                                        <h5 class="white-text">Descripción.</h5>
                                        <p class="grey-text text-lighten-4">Este es un proyecto, con fines educativos, 
                                            aplicando los conceptos del frameork spring MVC, con el framework materialize.</p>
                                    </div>
                                    <div class="col l4 offset-l2 s12">
                                        <h5 class="white-text">Links</h5>
                                        <ul>
                                            <li><a class="grey-text text-lighten-3" href="#!">Bancolombiano</a></li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="footer-copyright">
                                <div class="container">
                                    © 2018 Copyright Text
                                    <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
                                </div>
                            </div>
                        </footer>    
 </body>
</html>
